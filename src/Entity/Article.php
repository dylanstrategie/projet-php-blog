<?php

namespace App\Entity;

class Article {
    public $id;
    public $content;
    public $title;

    public function fromSQL(array $sql) {
        $this->id = $sql["id"];
        $this->content = $sql["content"];
        $this->title = $sql["title"];
    }
}