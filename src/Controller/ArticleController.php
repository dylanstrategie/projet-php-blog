<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;

class ArticleController extends AbstractController {

    /**
     * @Route("/add_article", name="add_article")
     */

    public function index(Request $request, ArticleRepository $repo)
    {
        //On crée le formulaire à partir de la classe Type qu'on a faite
        $form = $this->createForm(ArticleType::class);

        //On fait la suite comme avec un formulaire normale
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère l'instance de chien générée par le formulaire
            //avec getData() et on le donne à manger à la méthode
            //add du DogRepository qui fera persister le chien en question
            $repo->add($form->getData());
            //On fait une redirection lors d'un ajout réussi
            return $this->redirectToRoute("home");
        }

        return $this->render('addarticle.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/article/{id}", name="article")
     */
    public function update_article(int $id , ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }

        return $this->render('article.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

    /**
     * @Route("/remove_article/{id}", name="remove_article")
     */
    public function remove_article(int $id , ArticleRepository $repo, Request $request) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }

}