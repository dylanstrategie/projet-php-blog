<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;

class HomeController extends AbstractController {

    /**
     * @Route("/", name="home")
     */

    public function index(ArticleRepository $repo) {
        $articleList = $repo->getAll();
        return $this->render("homepage.html.twig", ['articleList' => $articleList]);
    }

}