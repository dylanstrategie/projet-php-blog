<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ConnectController extends AbstractController {

    /**
     * @Route("/connect", name="connectpage")
     */

    public function index() {
        return $this->render("connectpage.html.twig", []);
    }

}