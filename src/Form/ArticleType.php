<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use App\Entity\Article;
/**
 * Plutôt que de créer le formulaire directement dans les controleurs
 * symfony préconise de créer des classes représentant les formulaire
 * appelées Type.
 * Ici, on fait une classe SmallDogType dans laquelle on définira
 * les différents champs du formulaires ainsi que la classe à
 * laquelle le formulaire est liée
 */
class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //On définit les champs ici
        $builder
            ->add('title', TextType::class)
            ->add('content', TextareaType::class);
            //On ne met pas les boutons submit histoire de rendre
            //le formulaire le plus réutilisable possible
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        //On indique quelle classe le formulaire permet de créer
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}
