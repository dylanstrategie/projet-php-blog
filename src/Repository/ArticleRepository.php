<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\ConnectUtil;

class ArticleRepository {

    public function getAll():array {

        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");
        $articles = [];
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("SELECT * FROM article");
            $query->execute();

            foreach($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
            }
        }
        catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }

    public function add(Article $article) {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("INSERT INTO article (content, title) VALUES (:content, :title)");

            $query->bindValue(":content", $article->content);
            $query->bindValue(":title", $article->title);

            $query->execute();

            $article->id = intval($cnx->lastInsertId());
        }
        catch (\PDOException $e) {
            dump($e);
        }
    }

    public function update(Article $article) {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("UPDATE article SET title=:title, content=:content WHERE id=:id");

            $query->bindValue(":title", $article->title);
            $query->bindValue(":content", $article->content);
            $query->bindValue(":id", $article->id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id) {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("DELETE FROM article WHERE id = :id");

            $query->bindValue(":id", $id);

            $query->execute();

        }
        catch (\PDOException $e) {
            dump($e);
        }
    }

    public function getById(int $id): ?Article{
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("SELECT * FROM article WHERE id=:id");

            $query->bindValue(":id", $id);

            $query->execute();

            $result = $query->fetchAll();

            if(count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }

}